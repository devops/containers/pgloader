FROM debian:buster

RUN apt-get -y update \
	&& \
	apt-get -y install \
	less \
	libsqlite3-0 \
	pgloader \
	vim \
	&& \
	apt-get -y clean

RUN groupadd -g 1001 appuser \
	&& \
	useradd -u 1001 -m -g 1001 appuser

RUN mkdir -p -m 0755 /config \
	&& \
	chown -R 1001:1001 /config

VOLUME /config
USER 1001
